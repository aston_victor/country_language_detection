<?php

namespace Drupal\country_language_detection\Plugin\LanguageNegotiation;

use Drupal\language\LanguageNegotiationMethodBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying language via URL with country code.
 *
 * @LanguageNegotiation(
 *   id = Drupal\country_language_detection\Plugin\LanguageNegotiation\CountryLanguageDetection::METHOD_ID,
 *   weight = -100,
 *   name = @Translation("Country detection"),
 *   description = @Translation("Language based on the URL."),
 * )
 */
class CountryLanguageDetection extends LanguageNegotiationMethodBase implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * The language negotiation method id.
   */
  const METHOD_ID = 'country-detection';

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    $langcode = NULL;

    if ($request && $this->languageManager) {
      $languages = $this->languageManager->getLanguages();

      $request_path = urldecode(trim($request->getPathInfo(), '/'));
      $path_args = explode('/', $request_path);
      $prefix = array_shift($path_args);
      $detection = explode('-', $prefix);

      // Search prefix within added languages.
      if (!empty($detection[0]) && country_language_detection_check_country($detection[0])) {
        country_language_detection_tempstore_detection($prefix);

        $negotiated_language = FALSE;
        foreach ($languages as $language) {
          if (!empty($detection[1]) && $language->getId() == $detection[1]) {
            $negotiated_language = $language;
            break;
          }
        }

        if ($negotiated_language) {
          $langcode = $negotiated_language->getId();
        }
      }
    }

    return $langcode;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    // Search prefix within added languages.
    foreach ($this->languageManager->getLanguages() as $language) {
      $current_path = urldecode(trim($request->getPathInfo(), '/'));
      if (country_language_detection_check_path($current_path, $language)) {
        // Rebuild $path with the language removed.
        $parts = explode('/', trim($path, '/'));
        $prefix = array_shift($parts);

        country_language_detection_tempstore_detection($prefix);
        $path = '/' . implode('/', $parts);
        break;
      }
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    // Language can be passed as an option, or we go for current URL language.
    if (!isset($options['language'])) {
      $language_url = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_URL);
      $options['language'] = $language_url;
    }

    $data = country_language_detection_tempstore_detection();
    $options['prefix'] = !empty($data['detection']) ? $data['detection'] . '/' : NULL;

    if ($bubbleable_metadata) {
      // Remove cache for url's.
      $bubbleable_metadata->setCacheMaxAge(0);
    }

    return $path;
  }

}
